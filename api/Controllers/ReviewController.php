<?php

class ReviewController
{

    public function getReviews()
    {

        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM comentarios");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron reseñas']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function listReviews()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $fechaInicio = isset($_POST['fechaInicioReseñas']) ? $_POST['fechaInicioReseñas'] : null;
            $fechaFin = isset($_POST['fechaFinReseñas']) ? $_POST['fechaFinReseñas'] : null;

            // Valida las fechas
            if (empty($fechaInicio) || empty($fechaFin)) {
                echo "Error: Debes proporcionar ambas fechas.";
                exit();
            }

            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $pdo->prepare("SELECT A.username, B.com_descripcion, B.com_calificacion, B.com_fecha FROM usuarios A, comentarios B WHERE A.user_key = B.user_key AND b.com_fecha BETWEEN ? AND ?");
                $stmt->execute([$fechaInicio, $fechaFin]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);

                // Devuelve los resultados como un array indexado
                header('Content-Type: application/json');
                echo json_encode($resultados);
                exit();
            } catch (PDOException $e) {
                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }

    public function getLastsReviews()
    {
        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL con filtrado, ordenamiento y limitación
            $stmt = $pdo->query("SELECT * FROM (
                SELECT DISTINCT ON (user_key) * FROM comentarios
                WHERE com_calificacion >= 4
                ORDER BY user_key, com_fecha DESC, com_calificacion DESC) 
                AS subquery ORDER BY com_fecha DESC LIMIT 3");

            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($response) {
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron reseñas']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function postReviews()
    {
        session_start();

        if (!isset($_SESSION['usuario'])) {
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $userId = $_SESSION['user_key'];
            $review = isset($_POST['comentarioTestimonio']) ? $_POST['comentarioTestimonio'] : '';
            $calification = isset($_POST['calificacionTestimonio']) ? floatval($_POST['calificacionTestimonio']) : 0.0;
            $date = date("Y-m-d");

            if (empty($calification) || empty($review)) {
                echo "Error: Todos los campos son obligatorios.";
                exit();
            }

            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Utiliza una consulta preparada para prevenir la inyección de SQL
                $stmt = $pdo->prepare("INSERT INTO comentarios (user_key, com_descripcion, com_calificacion, com_fecha) VALUES (?, ?, ?, ?)");

                // Asocia los valores a los parámetros de la consulta preparada
                $stmt->execute([$userId, $review, $calification, $date]);

                header('Location: /home#testimonial-section');
                exit();
            } catch (PDOException $e) {
                // Maneja los errores de la base de datos
                echo "Error al insertar en la base de datos: " . $e->getMessage();
            }
        }
    }
}
