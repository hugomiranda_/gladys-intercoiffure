<?php

class RegisterController {

    public function registerUser()
    {
        // Verifica si se reciben datos del formulario
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Verifica si todas las claves necesarias están presentes en $_POST
            if (isset($_POST['nombre'], $_POST['usuario'], $_POST['contrasena'], $_POST['email'])) {
                $nombre = $_POST['nombre'];
                $usuario = $_POST['usuario'];
                $contrasena = password_hash($_POST['contrasena'], PASSWORD_BCRYPT); // Cifra la contraseña
                $email = $_POST['email'];
                $fechaRegistro = date('Y-m-d'); // Obtiene la fecha y hora actual
                
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $password = $_ENV['DB_PASSWORD'];
                
                try {
                    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
                    // Realiza la consulta SQL para verificar si el usuario ya existe
                    $stmt = $pdo->prepare("SELECT * FROM usuarios WHERE LOWER(username) = LOWER(:usuario)");
                    $stmt->bindParam(':usuario', $usuario);
                    $stmt->execute();
                    
                    // Verifica si el usuario ya existe
                    if ($stmt->rowCount() > 0) {
                        echo "Error: El nombre de usuario ya está en uso.";
                        exit();
                    }
                
                    // Si el usuario no existe, realiza la inserción en la base de datos
                    $stmt = $pdo->prepare("INSERT INTO usuarios (username, password, user_nombre, user_email, user_fechaini) VALUES (:usuario, :contrasena, :nombre, :email, :fechaRegistro)");
                    $stmt->bindParam(':nombre', $nombre);
                    $stmt->bindParam(':usuario', $usuario);
                    $stmt->bindParam(':contrasena', $contrasena);
                    $stmt->bindParam(':email', $email);
                    $stmt->bindParam(':fechaRegistro', $fechaRegistro);
                    $stmt->execute();
                
                    // Redirige al usuario después del registro
                    header('Location: /mainLogin');
                    exit();
                } catch (PDOException $e) {
                    http_response_code(500);
                    echo "Error en la conexión a la base de datos: " . $e->getMessage();
                    exit();
                }
            }
            
            echo "Error: Todos los campos son obligatorios.";
            exit();
        } else {
            // Redirige al formulario de registro si se intenta acceder directamente al controlador
            header('Location: /register');
            exit();
        }
    }
}
