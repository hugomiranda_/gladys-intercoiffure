<?php

class ServiceController
{

    public function getServices()
    {

        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM servicios WHERE ser_estado = '1'");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron servicios']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function getUsers()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        // Consulta los datos en la base de datos
        try {
            $host = $_ENV['DB_HOST'];
            $dbname = $_ENV['DB_DATABASE'];
            $user = $_ENV['DB_USERNAME'];
            $dbPassword = $_ENV['DB_PASSWORD'];
    
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
            $stmt = $pdo->query("SELECT user_key, username FROM usuarios WHERE user_funcionario = '1' AND user_estado = '1' ORDER BY user_key");
            $funcionarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // Verifica si hay datos antes de imprimir el JSON
            if ($funcionarios) {
                // Devuelve los datos de los funcionarios en formato JSON
                header('Content-Type: application/json');
                echo json_encode($funcionarios);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron funcionarios']);
            }
    
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }
    

    public function postServices()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            print_r($_POST);
            // Verifica la presencia de los campos en $_POST
            // Obtiene el valor del campo oculto userkey
            $name = isset($_POST['nombreServicio']) ? $_POST['nombreServicio'] : null;
            $type = isset($_POST['tipoServicio']) ? $_POST['tipoServicio'] : null;
            $price = isset($_POST['precioServicio']) ? $_POST['precioServicio'] : null;
            $commission = isset($_POST['comisionServicio']) ? $_POST['comisionServicio'] : null;
            $description = isset($_POST['descripcionServicio']) ? $_POST['descripcionServicio'] : null;
            $time = isset($_POST['horaEstiServicio']) ? $_POST['horaEstiServicio'] : null;
            $promo = isset($_POST['promocionServicio']) ? $_POST['promocionServicio'] : null;
            $startDate = isset($_POST['fechaIniServicio']) ? $_POST['fechaIniServicio'] : null;

            if (empty($name) || empty($type) || empty($price) || empty($commission) || empty($description) || empty($time) ||  $promo === null || empty($startDate) ) {
                echo "Error: Todos los campos son obligatorios.";
                exit();
            }

            // Inserta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Utiliza una consulta preparada para prevenir la inyección de SQL
                $stmt = $pdo->prepare("INSERT INTO servicios (ser_nombre, ser_tipo, ser_precio, ser_comision, ser_descripcion, ser_horaesti, ser_estado, ser_promocion, ser_fechaini) VALUES (?, ?, ?, ?, ?, ?, 1, ?, ?)");

                // Asocia los valores a los parámetros de la consulta preparada
                $stmt->execute([$name, $type, $price, $commission, $description, $time, $promo, $startDate]);

                header('Location: /admin');
                exit();
            } catch (PDOException $e) {
                // Maneja los errores de la base de datos
                echo "Error al insertar en la base de datos: " . $e->getMessage();
            }
        }
    }

    public function listServices()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
            $fechaInicio = isset($_POST['fechaInicioServicios']) ? $_POST['fechaInicioServicios'] : null;
            $fechaFin = isset($_POST['fechaFinServicios']) ? $_POST['fechaFinServicios'] : null;
    
            // Valida las fechas
            if (empty($fechaInicio) || empty($fechaFin)) {
                echo "Error: Debes proporcionar ambas fechas.";
                exit();
            }
    
            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];
    
                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
                $stmt = $pdo->prepare("
                SELECT  ser_nombre,
                        ser_tipo,
						ser_descripcion,
                        ser_precio,
                        ser_comision,
                        ser_horaesti,
                        CASE ser_promocion
                            WHEN '1' THEN 'Si'
                            ELSE 'No'
                        END ser_promocion,
                            ser_fechaini
                FROM servicios 
                WHERE ser_fechaini BETWEEN ? AND ?              
                ");
                $stmt->execute([$fechaInicio, $fechaFin]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // Devuelve los resultados como un array indexado
                header('Content-Type: application/json');
                echo json_encode($resultados);
                exit();
    
            } catch (PDOException $e) {
                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }
    

}

