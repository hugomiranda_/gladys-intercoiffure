<?php

class PromotionController
{

    public function getPromotions()
    {

        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM promociones WHERE pro_estado = '1'");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron promociones']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function getNameServices()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        // Consulta los datos en la base de datos
        try {
            $host = $_ENV['DB_HOST'];
            $dbname = $_ENV['DB_DATABASE'];
            $user = $_ENV['DB_USERNAME'];
            $dbPassword = $_ENV['DB_PASSWORD'];
    
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
            $stmt = $pdo->query("SELECT ser_key, ser_nombre FROM servicios WHERE ser_estado = '1' ORDER BY ser_key");
            $servicios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // Verifica si hay datos antes de imprimir el JSON
            if ($servicios) {
                // Devuelve los datos de los funcionarios en formato JSON
                header('Content-Type: application/json');
                echo json_encode($servicios);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron funcionarios']);
            }
    
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function postPromotions()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            print_r($_POST);
            // Verifica la presencia de los campos en $_POST
            // Obtiene el valor del campo oculto userkey
            $serKey = isset($_POST['servicioPromocion']) ? $_POST['servicioPromocion'] : null;
            $name = isset($_POST['nombrePromocion']) ? $_POST['nombrePromocion'] : null;
            $type = isset($_POST['tipoPromocion']) ? $_POST['tipoPromocion'] : null;
            $price = isset($_POST['precioPromocion']) ? $_POST['precioPromocion'] : null;
            $discount = isset($_POST['descuentoPromocion']) ? $_POST['descuentoPromocion'] : null;
            $description = isset($_POST['descripcionPromocion']) ? $_POST['descripcionPromocion'] : null;
            $startDate = isset($_POST['fechaIniPromocion']) ? $_POST['fechaIniPromocion'] : null;
            $endDate = isset($_POST['fechaFinPromocion']) ? $_POST['fechaFinPromocion'] : null;

            if (empty($serKey) || empty($name) || empty($type) || empty($price) || empty($discount) 
                || $description === null || empty($startDate) || empty($endDate)) {
                echo "Error: Todos los campos son obligatorios.";
                exit();
            }

            // Inserta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Utiliza una consulta preparada para prevenir la inyección de SQL
                $stmt = $pdo->prepare("INSERT INTO promociones (ser_key, pro_nombre, pro_tipo, pro_precio, pro_descuento, pro_descripcion, pro_estado, pro_fechaini, pro_fechafin) VALUES (?, ?, ?, ?, ?, ?, 1, ?, ?)");

                // Asocia los valores a los parámetros de la consulta preparada
                $stmt->execute([$serKey, $name, $type, $price, $discount, $description, $startDate, $endDate]);

                header('Location: /admin');
                exit();
            } catch (PDOException $e) {
                // Maneja los errores de la base de datos
                echo "Error al insertar en la base de datos: " . $e->getMessage();
            }
        }
    }


    public function listPromotions()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
            $fechaInicio = isset($_POST['fechaInicioPromociones']) ? $_POST['fechaInicioPromociones'] : null;
            $fechaFin = isset($_POST['fechaFinPromociones']) ? $_POST['fechaFinPromociones'] : null;
    
            // Valida las fechas
            if (empty($fechaInicio) || empty($fechaFin)) {
                echo "Error: Debes proporcionar ambas fechas.";
                exit();
            }
    
            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];
    
                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
                $stmt = $pdo->prepare("SELECT A.ser_nombre, B.pro_nombre, B.pro_tipo, B.pro_descuento, B.pro_precio, B.pro_descripcion, B.pro_fechaini, B.pro_fechafin FROM servicios A, promociones B WHERE A.ser_key = B.ser_key AND B.pro_fechaini BETWEEN ? AND ?");
                $stmt->execute([$fechaInicio, $fechaFin]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // Devuelve los resultados como un array indexado
                header('Content-Type: application/json');
                echo json_encode($resultados);
                exit();
    
            } catch (PDOException $e) {
                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }
    

}

