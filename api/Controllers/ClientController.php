<?php

class ClientController
{

    public function getClients()
    {
        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM usuarios WHERE user_estado = '1' and user_funcionario = '0'");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron servicios']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }
}
