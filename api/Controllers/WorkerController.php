<?php

class WorkerController
{

    public function getWorkers()
    {

        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM usuarios WHERE user_funcionario = '1' and user_descripcion != 'Administrador' order by user_key");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron trabajadores']);
                return;
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function getWorkersWithCommission()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        // Consulta los datos en la base de datos
        try {
            $host = $_ENV['DB_HOST'];
            $dbname = $_ENV['DB_DATABASE'];
            $user = $_ENV['DB_USERNAME'];
            $dbPassword = $_ENV['DB_PASSWORD'];
    
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
            $stmt = $pdo->query("
            SELECT DISTINCT A.user_key, 
                            A.username 
            FROM usuarios A, citas B 
            WHERE A.user_funcionario = '1' 
            AND A.user_estado = '1'
            AND A.user_key = B.user_key
            ORDER BY A.user_key");
            $funcionarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // Verifica si hay datos antes de imprimir el JSON
            if ($funcionarios) {
                // Devuelve los datos de los funcionarios en formato JSON
                header('Content-Type: application/json');
                echo json_encode($funcionarios);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron funcionarios con citas']);
            }
    
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function postWorkers()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            print_r($_POST);
            // Verifica la presencia de los campos en $_POST
            $username = isset($_POST['usuarioFuncionarios']) ? $_POST['usuarioFuncionarios'] : null;
            $password = isset($_POST['contraseñaFuncionarios']) ? password_hash($_POST['contraseñaFuncionarios'], PASSWORD_BCRYPT) : null;
            $name = isset($_POST['nombreFuncionarios']) ? $_POST['nombreFuncionarios'] : null;
            $phoneNumber = isset($_POST['telefonoFuncionarios']) ? $_POST['telefonoFuncionarios'] : null;
            $email = isset($_POST['correoFuncionarios']) ? $_POST['correoFuncionarios'] : null;
            $description = isset($_POST['descripcionFuncionarios']) ? $_POST['descripcionFuncionarios'] : null;
            $startDate = isset($_POST['fechaIniFuncionarios']) ? $_POST['fechaIniFuncionarios'] : null;

            if (empty($username) || empty($password) || empty($name) || empty($phoneNumber) || empty($email) || empty($description) || empty($startDate)) {
                echo "Error: Completa los campos requeridos.";
                exit();
            }

            // Inserta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Utiliza una consulta preparada para prevenir la inyección de SQL
                $stmt = $pdo->prepare("INSERT INTO usuarios (username, password, user_nombre, user_telef, user_email, user_descripcion, user_fechaini, user_funcionario, user_estado) VALUES (?, ?, ?, ?, ?, ?, ?, 1, 1)");

                // Asocia los valores a los parámetros de la consulta preparada
                $stmt->execute([$username, $password, $name, $phoneNumber, $email, $description, $startDate]);

                header('Location: /admin');
                exit();
            } catch (PDOException $e) {
                echo "Error al insertar en la base de datos: " . $e->getMessage();
            }
        }
    }

    public function listWorkers()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {

            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $fechaInicio = isset($_POST['fechaInicioFuncionarios']) ? $_POST['fechaInicioFuncionarios'] : null;
            $fechaFin = isset($_POST['fechaFinFuncionarios']) ? $_POST['fechaFinFuncionarios'] : null;

            // Valida las fechas
            if (empty($fechaInicio) || empty($fechaFin)) {
                echo "Error: Debes proporcionar ambas fechas.";
                exit();
            }

            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $pdo->prepare("
                SELECT  username,
	                    user_nombre,
	                    user_telef,
	                    user_email,
	                    user_descripcion,
                        CASE user_estado
                            WHEN '1' THEN 'Activo'
                            ELSE 'Inactivo'
                        END user_estado,
	                        user_fechaini,
	                        user_fechafin
                FROM usuarios
                WHERE user_fechaini BETWEEN ? AND ?
                ORDER BY user_key
                ");
                $stmt->execute([$fechaInicio, $fechaFin]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);

                // Muestra los resultados en la tabla HTML en formato JSON
                echo json_encode($resultados);
                exit();
            } catch (PDOException $e) {

                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }

    public function listCommissionWorkers()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {

            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $userKey = isset($_POST['usuarioComision']) ? $_POST['usuarioComision'] : null;

            // Valida el usuario
            if (empty($userKey)) {
                echo "Error: Debes proporcionar un usuario.";
                exit();
            }

            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $pdo->prepare("
                SELECT
                        A.username,
                        B.ser_nombre,
                        B.ser_tipo,
                        B.ser_precio,
                        B.ser_comision,
                        CASE B.ser_promocion
                            WHEN '1' THEN 'Si'
                            ELSE 'No'
                        END AS ser_promocion,
                        CASE 
                            WHEN B.ser_promocion = '0' 
                                THEN (
                                    SELECT ROUND(C1.ser_precio * C1.ser_comision)::text 
                                    FROM servicios C1
                                    WHERE C1.ser_key = B.ser_key
                                )
                            ELSE 'No aplica'
                        END AS comision_servicio,
                        CASE 
                            WHEN B.ser_promocion = '1' 
                                THEN (
                                    SELECT ROUND(CAST(ROUND(C1.ser_precio * P1.pro_descuento) * C1.ser_comision AS numeric))::text 
                                    FROM servicios C1, promociones P1 
                                    WHERE C1.ser_key = P1.ser_key AND C1.ser_key = B.ser_key
                                )
                            ELSE 'No aplica'
                        END AS comision_promocion,
                        D.cita_fecha
                FROM
                    usuarios A, citas D, servicios B, promociones C 
                WHERE D.user_key = ?
                AND D.user_key = A.user_key
                AND D.ser_key = B.ser_key
                AND B.ser_key = C.ser_key
                ");
                $stmt->execute([$userKey]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);

                // Muestra los resultados en la tabla HTML en formato JSON
                echo json_encode($resultados);
                exit();
            } catch (PDOException $e) {

                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }
}

