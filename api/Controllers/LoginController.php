<?php

class LoginController {

    public function showLogin() {
        include './index.html';
    }

    public function loginUser()
    {
        session_start(); // Inicia la sesión

        // Verifica si se reciben datos del formulario
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Verifica si las claves 'usuario' y 'contrasena' están presentes en $_POST
            if (isset($_POST['usuario'], $_POST['contrasena'])) {
                $usuario = $_POST['usuario'];
                $contrasena = $_POST['contrasena'];

                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $password = $_ENV['DB_PASSWORD'];

                try {
                    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    // Realiza la consulta SQL para validar el inicio de sesión
                    $stmt = $pdo->prepare("SELECT * FROM usuarios WHERE username = :usuario");
                    $stmt->bindParam(':usuario', $usuario);
                    $stmt->execute();

                    // Verifica si se encontró un usuario
                    if ($stmt->rowCount() > 0) {
                        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

                        // Verifica la contraseña cifrada utilizando password_verify
                        if (password_verify($contrasena, $userRow['password'])) {
                            // Inicia la sesión y redirige al home
                            $_SESSION['usuario'] = $usuario;
                            $_SESSION['user_funcionario'] = $userRow['user_funcionario'];
                            $_SESSION['user_nombre'] = $userRow['user_nombre'];
                            $_SESSION['user_key'] = $userRow['user_key'];

                            header('Location: /home');
                            exit();
                        }
                    }
                } catch (PDOException $e) {
                    http_response_code(500);
                    echo "Error en la conexión a la base de datos: " . $e->getMessage();
                    exit();
                }
            }

            echo "Error: Usuario o contraseña incorrectos.";
            exit();
        } else {
            header('Location: /login');
            exit();
        }
    }

    public function logoutUser() {
        // Inicia la sesión
        session_start();

        // Cierra la sesión
        session_unset(); // Elimina todas las variables de sesión
        session_destroy(); // Destruye la sesión
        session_regenerate_id(true); // Regenera la ID de sesión y elimina la información de la sesión actual

        // Redirige a la página de login después del logout
        header('Location: /mainLogin');
        exit();
    }
}

?>
