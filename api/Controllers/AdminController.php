<?php

class AdminController
{
    public function showAdminHome()
    {
        // Inicia la sesión
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        // El usuario está autenticado y es un funcionario, muestra la vista del admin
        include './resources/views/admin.php';
    }
}
