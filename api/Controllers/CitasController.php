<?php

class CitasController
{

    public function getCitas()
    {

        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_DATABASE'];
        $user = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Realiza la consulta SQL para obtener empleados
            $stmt = $pdo->query("SELECT * FROM citas");
            $response = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Verifica si hay datos antes de imprimir el JSON
            if ($response) {
                // Devuelve los datos en formato JSON
                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                http_response_code(404);
                echo json_encode(['error' => 'No se encontraron horarios']);
            }
        } catch (PDOException $e) {
            http_response_code(500);
            echo "Error en la conexión a la base de datos: " . $e->getMessage();
        }
    }

    public function postCitas()
    {
        session_start();

        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            print_r($_POST);
            // Verifica la presencia de los campos en $_POST
            // Obtiene el valor del campo oculto userkey
            $userKey = isset($_POST['usuarioCita']) ? $_POST['usuarioCita'] : null;
            $serviceKey = isset($_POST['servicioCita']) ? $_POST['servicioCita'] : null;
            $date = isset($_POST['fechaCita']) ? $_POST['fechaCita'] : null;
            $startTime = isset($_POST['horaInicioCita']) ? $_POST['horaInicioCita'] : null;
            $endTime = isset($_POST['horaFinCita']) ? $_POST['horaFinCita'] : null;
            

            if (empty($userKey) || empty($serviceKey) || empty($date) || empty($startTime) || empty($endTime)) {
                echo "Error: Todos los campos son obligatorios.";
                exit();
            }

            // Inserta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];

                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Utiliza una consulta preparada para prevenir la inyección de SQL
                $stmt = $pdo->prepare("INSERT INTO citas (user_key, ser_key, cita_fecha, cita_horaini, cita_horafin) VALUES (?, ?, ?, ?, ?)");

                // Asocia los valores a los parámetros de la consulta preparada
                $stmt->execute([$userKey, $serviceKey, $date, $startTime, $endTime]);

                header('Location: /admin');
                exit();
            } catch (PDOException $e) {
                // Maneja los errores de la base de datos
                echo "Error al insertar en la base de datos: " . $e->getMessage();
            }
        }
    }

    public function listCitas()
    {
        session_start();
    
        // Verifica si el usuario está autenticado y es funcionario
        if (!isset($_SESSION['usuario']) || $_SESSION['user_funcionario'] != 1) {
            // Redirige al login si el usuario no está autenticado o no es un funcionario
            header('Location: /mainLogin');
            exit();
        }
    
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
            $fechaInicio = isset($_POST['fechaInicioCitas']) ? $_POST['fechaInicioCitas'] : null;
            $fechaFin = isset($_POST['fechaFinCitas']) ? $_POST['fechaFinCitas'] : null;
    
            // Valida las fechas
            if (empty($fechaInicio) || empty($fechaFin)) {
                echo "Error: Debes proporcionar ambas fechas.";
                exit();
            }
    
            // Consulta los datos en la base de datos
            try {
                $host = $_ENV['DB_HOST'];
                $dbname = $_ENV['DB_DATABASE'];
                $user = $_ENV['DB_USERNAME'];
                $dbPassword = $_ENV['DB_PASSWORD'];
    
                $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $dbPassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
                $stmt = $pdo->prepare("
                SELECT  A.username,
	                    B.ser_nombre,
						C.cita_fecha,
                        C.cita_horaini,
                        C.cita_horafin
                FROM usuarios A, servicios B, citas C
                WHERE cita_fecha BETWEEN ? AND ?
                AND A.user_key = C.user_key
                AND B.ser_key = C.ser_key
                ");
                $stmt->execute([$fechaInicio, $fechaFin]);
                $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // Devuelve los resultados como un array indexado
                header('Content-Type: application/json');
                echo json_encode($resultados);
                exit();
    
            } catch (PDOException $e) {
                echo "Error al consultar la base de datos: " . $e->getMessage();
            }
        }
    }
    

}

