<?php
require 'router.php';
require 'api/Controllers/WorkerController.php';
require 'api/Controllers/HomeController.php';
require 'api/Controllers/AdminController.php';
require 'api/Controllers/RegisterController.php';
require 'api/Controllers/LoginController.php';
require 'api/Controllers/ServiceController.php';
require 'api/Controllers/PromotionController.php';
require 'api/Controllers/CitasController.php';
require 'api/Controllers/ReviewController.php';
require 'api/Controllers/ClientController.php';

$router = new Router();

// Rutas - LoginController
$router->addRoute('GET', '/mainLogin', 'LoginController@showLogin');
$router->addRoute('POST', '/login', 'LoginController@loginUser');
$router->addRoute('POST', '/logout', 'LoginController@logoutUser');
$router->addRoute('GET', '/logoutMenu', 'LoginController@logoutUser');

// Rutas - RegisterController
$router->addRoute('POST', '/register', 'RegisterController@registerUser');

// Rutas - HomeController
$router->addRoute('GET', '/home', 'HomeController@showHome');

// Rutas - AdminController
$router->addRoute('GET', '/admin', 'AdminController@showAdminHome');

// Rutas - UserController
$router->addRoute('GET', '/getClients', 'ClientController@getClients');

// Rutas - WorkerController
$router->addRoute('GET', '/workers', 'WorkerController@getWorkers');
$router->addRoute('GET', '/workersCommission', 'WorkerController@getWorkersWithCommission');
$router->addRoute('POST', '/postWorkers', 'WorkerController@postWorkers');
$router->addRoute('POST', '/listWorkers', 'WorkerController@listWorkers');
$router->addRoute('POST', '/listCommissionWorkers', 'WorkerController@listCommissionWorkers');


// Rutas - ServiceController
$router->addRoute('GET', '/services', 'ServiceController@getServices');
$router->addRoute('GET', '/getUsers', 'ServiceController@getUsers');
$router->addRoute('POST', '/postServices', 'ServiceController@postServices');
$router->addRoute('POST', '/listServices', 'ServiceController@listServices');

// Rutas - PromotionController
$router->addRoute('GET', '/promotions', 'PromotionController@getPromotions');
$router->addRoute('GET', '/getServices', 'PromotionController@getNameServices');
$router->addRoute('POST', '/postPromotions', 'PromotionController@postPromotions');
$router->addRoute('POST', '/listPromotions', 'PromotionController@listPromotions');

// Rutas - CitasController
$router->addRoute('GET', '/citas', 'CitasController@getCitas');
$router->addRoute('POST', '/postCitas', 'CitasController@postCitas');
$router->addRoute('POST', '/listCitas', 'CitasController@listCitas');

// Rutas - ReviewController
$router->addRoute('GET', '/reviews', 'ReviewController@getReviews');
$router->addRoute('POST', '/listReviews', 'ReviewController@listReviews');
$router->addRoute('GET', '/lastReviews', 'ReviewController@getLastsReviews');
$router->addRoute('POST', '/postReviews', 'ReviewController@postReviews');

// Manejar la solicitud
$router->handleRequest();
