<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/resources/css/styles_admin.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.5/xlsx.full.min.js"></script>
  <link rel="icon" href="data:;base64,iVBORw0KGgo=">
  <title>Menú Administrador</title>
</head>

<body>

  <nav>
    <button class="boton-usuario">
      <?php echo $_SESSION['usuario']; ?>
      <ul class="submenu-usuario">
        <li>
          <form action="/home">
            <a href="/home">Volver atrás</a>
          </form>
        </li>
        <li>
          <form action="/logoutMenu" method="post">
            <a href="/logoutMenu">Cerrar sesión</a>
          </form>
        </li>
      </ul>
    </button>
    <h1 class="titulo">Gladys Intercoiffure</h1>

    <ul>
      <li>
        <a href="#funcionarios">Funcionarios</a>
        <ul class="submenu">
          <li class="has-submenu" data-formulario="cargarFuncionarios"><a href="#cargarFuncionarios">Cargar Funcionarios</a></li>
          <li class="has-submenu" data-formulario="listarFuncionarios"><a href="#listarFuncionarios">Listar Funcionarios</a></li>
          <li class="has-submenu" data-formulario="listarComisionFuncionarios"><a href="#listarComisionFuncionarios">Listar Comisión Funcionarios</a></li>
        </ul>
      </li>

      <li>
        <a href="#servicios">Servicios</a>
        <ul class="submenu">
          <li class="has-submenu" data-formulario="cargarServicios"><a href="#cargarServicios">Cargar Servicios</a></li>
          <li class="has-submenu" data-formulario="listarServicios"><a href="#listarServicios">Listar Servicios</a></li>
        </ul>
      </li>

      <li>
        <a href="#promociones">Promociones</a>
        <ul class="submenu">
          <li class="has-submenu" data-formulario="cargarPromociones"><a href="#cargarPromociones">Cargar Promociones</a></li>
          <li class="has-submenu" data-formulario="listarPromociones"><a href="#listarPromociones">Listar Promociones</a></li>
        </ul>
      </li>

      <li>
        <a href="#citas">Citas</a>
        <ul class="submenu">
          <li class="has-submenu" data-formulario="cargarCitas"><a href="#cargarCitas">Cargar Citas</a></li>
          <li class="has-submenu" data-formulario="listarCitas"><a href="#listarCitas">Listar Citas</a></li>
        </ul>
      </li>

      <li>
        <a href="#Reseñas">Reseñas</a>
        <ul class="submenu">
          <li class="has-submenu" data-formulario="listarReseñas"><a href="#listarReseñas">Listar Reseñas</a></li>
        </ul>
      </li>

    </ul>
  </nav>
  <section id="contenido">
    <?php
    if (isset($_SESSION['usuario'])) {
      // El usuario está autenticado, muestra la página con el botón de logout
      echo '<h2 class="titulo-welcome">¡Bienvenido ' . $_SESSION['user_nombre'] . '!</h2>';
    }
    ?>
    <h2 class="titulo-menu-admin">Menú Administrador</h2>
    <img src="/resources/img/admin/alexander aronowitz 1.png" alt="Logo Peluquería">

    <!-- Formulario de carga de Funcionarios -->
    <div id="cargarFuncionarios" class="formulario-carga">
      <div class="formulario-contenedor-cargar">
        <h2>Registrar Funcionario</h2>
        <form id="formCargarFuncionarios" action="/postWorkers" method="POST" enctype="multipart/form-data">
          <label for="usuarioFuncionarios">Usuario:</label>
          <input type="text" id="usuarioFuncionarios" name="usuarioFuncionarios" required>

          <label for="contraseñaFuncionarios">Contraseña:</label>
          <input type="password" id="contraseñaFuncionarios" name="contraseñaFuncionarios" required>

          <label for="nombreFuncionarios">Nombre:</label>
          <input type="text" id="nombreFuncionarios" name="nombreFuncionarios" required>

          <label for="telefonoFuncionarios">Celular:</label>
          <input type="text" id="telefonoFuncionarios" name="telefonoFuncionarios" required>

          <label for="correoFuncionarios">Correo:</label>
          <input type="text" id="correoFuncionarios" name="correoFuncionarios" required>

          <label for="descripcionFuncionarios">Descripción:</label>
          <input type="text" id="descripcionFuncionarios" name="descripcionFuncionarios" required>

          <label for="fechaIniFuncionarios">Fecha Inicio:</label>
          <input type="date" id="fechaIniFuncionarios" name="fechaIniFuncionarios" required>
  
          <button type="submit">Registrar</button>
          <button type="button" id="cancelarFormularioFuncionarios">Cancelar</button>
        </form>
      </div>
    </div>

    <div id="listarFuncionarios" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Funcionarios</h2>
        <label for="fechaInicioFuncionarios">Fecha Inicio:</label>
        <input type="date" id="fechaInicioFuncionarios" required>
        <label for="fechaFinFuncionarios">Fecha Fin:</label>
        <input type="date" id="fechaFinFuncionarios" required>
        <button type="button" id="listarFuncionario">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarFuncionarios">Cancelar</button>
        <button type="button" id="exportarFuncionarios">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaFuncionarios">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Email</th>
              <th>Descripción</th>
              <th>Estado</th>
              <th>Fecha Inicio</th>
              <th>Fecha Fin</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

    <div id="listarComisionFuncionarios" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Comisión Funcionarios</h2>
        <label for="usuarioComision">Usuario:</label>
        <select id="usuarioComision" name="usuarioComision" required>
            <?php
            // Agrega opciones para cada funcionario obtenido desde la base de datos
            foreach ($funcionario as $funcionario) {
              echo '<option value="' . $funcionario['user_key'] . '">' . $funcionario['username'] . '</option>';
            }
            ?>
        </select><br>
        <!-- Nuevo input hidden para almacenar el user_key -->
        <input type="hidden" id="userKeyComision" name="userKeyComision" value="">

        <button type="button" id="listarComisionFuncionario">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarComisionFuncionarios">Cancelar</button>
        <button type="button" id="exportarComisionFuncionarios">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaComisionFuncionarios">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Servicio</th>
              <th>Tipo</th>
              <th>Precio</th>
              <th>Comisión</th>
              <th>Promoción</th>
              <th>Comisión Servicio</th>
              <th>Comisión Promoción</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Formulario de carga de Promociones -->
    <div id="cargarServicios" class="formulario-carga">
      <div class="formulario-contenedor-cargar">
        <h2>Cargar Servicios</h2>
        <form id="formCargarServicios" action="/postServices" method="POST">

          <label for="nombreServicio">Nombre:</label>
          <input type="text" id="nombreServicio" name="nombreServicio" required>

          <label for="tipoServicio">Tipo:</label>
          <input type="text" id="tipoServicio" name="tipoServicio" required>

          <label for="precioServicio">Precio:</label>
          <input type="number" id="precioServicio" name="precioServicio" required>

          <label for="comisionServicio">Comisión:</label>
          <input type="number" id="comisionServicio" name="comisionServicio" required>

          <label for="descripcionServicio">Descripción:</label>
          <input type="text" id="descripcionServicio" name="descripcionServicio" required>

          <label for="horaEstiServicio">Hora Estimada:</label>
          <input type="number" id="horaEstiServicio" name="horaEstiServicio" required>

          <label for="promocionServicio">Promoción:</label>
          <select id="promocionServicio" name="promocionServicio" required>
            <option value="1">Si</option>
            <option value="0">No</option>
          </select>

          <label for="fechaIniServicio">Fecha Servicio:</label>
          <input type="date" id="fechaIniServicio" name="fechaIniServicio" required>

          <button type="submit">Registrar</button>
          <button type="button" id="cancelarFormularioServicios">Cancelar</button>
        </form>
      </div>
    </div>


    <div id="listarServicios" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Servicios</h2>
        <label for="fechaInicioServicios">Fecha Inicio:</label>
        <input type="date" id="fechaInicioServicios" required>
        <label for="fechaFinServicios">Fecha Fin:</label>
        <input type="date" id="fechaFinServicios" required>
        <button type="button" id="listarServicio">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarServicios">Cancelar</button>
        <button type="button" id="exportarServicios">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaServicios">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Tipo</th>
              <th>Descripción</th>
              <th>Precio</th>
              <th>Comisión</th>
              <th>Hora Estimada</th>
              <th>Promoción</th>
              <th>Fecha Servicio</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>


    <!-- Formulario de carga de Promociones -->
    <div id="cargarPromociones" class="formulario-carga">
      <div class="formulario-contenedor-cargar">
        <h2>Cargar Promociones</h2>
        <form id="formCargarPromociones" action="/postPromotions" method="POST">
          <!-- Dropdown de usuarios -->
          <label for="servicioPromocion">Servicio:</label>
          <select id="servicioPromocion" name="servicioPromocion" required>
            <?php
            // Agrega opciones para cada funcionario obtenido desde la base de datos
            foreach ($servicios as $servicio) {
              echo '<option value="' . $servicio['ser_key'] . '">' . $servicio['ser_nombre'] . '</option>';
            }
            ?>
          </select>

          <!-- Nuevo input hidden para almacenar el user_key -->
          <input type="hidden" id="serKeyPromocion" name="serKeyPromocion" value="">

          <label for="nombrePromocion">Nombre:</label>
          <input type="text" id="nombrePromocion" name="nombrePromocion" required>

          <label for="tipoPromocion">Tipo:</label>
          <select id="tipoPromocion" name="tipoPromocion" required>
            <option value="2x1">2x1</option>
            <option value="Descuento">Descuento</option>
          </select>

          <label for="precioPromocion">Precio:</label>
          <input type="number" id="precioPromocion" name="precioPromocion" required>

          <label for="descuentoPromocion">Descuento:</label>
          <input type="number" id="descuentoPromocion" name="descuentoPromocion" required>

          <label for="descripcionPromocion">Descripción:</label>
          <input type="text" id="descripcionPromocion" name="descripcionPromocion" required>

          <label for="fechaIniPromocion">Fecha Inicio:</label>
          <input type="date" id="fechaIniPromocion" name="fechaIniPromocion" required>

          <label for="fechaFinPromocion">Fecha Fin:</label>
          <input type="date" id="fechaFinPromocion" name="fechaFinPromocion" required>

          <button type="submit">Registrar</button>
          <button type="button" id="cancelarFormularioPromociones">Cancelar</button>
        </form>
      </div>
    </div>


    <div id="listarPromociones" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Promociones</h2>
        <label for="fechaInicioPromociones">Fecha Inicio:</label>
        <input type="date" id="fechaInicioPromociones" required>
        <label for="fechaFinPromociones">Fecha Fin:</label>
        <input type="date" id="fechaFinPromociones" required>
        <button type="button" id="listarPromocion">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarPromociones">Cancelar</button>
        <button type="button" id="exportarPromociones">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaPromociones">
          <thead>
            <tr>
              <th>Servicio</th>
              <th>Nombre</th>
              <th>Tipo</th>
              <th>Descuento</th>
              <th>Precio</th>
              <th>Descripción</th>
              <th>Fecha Inicio</th>
              <th>Fecha Fin</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Formulario de carga de Promociones -->
    <div id="cargarCitas" class="formulario-carga">
      <div class="formulario-contenedor-cargar">
        <h2>Cargar Citas</h2>
        <form id="formCargarCitas" action="/postCitas" method="POST">
          <!-- Dropdown de usuarios -->
          <label for="usuarioCita">Usuario:</label>
          <select id="usuarioCita" name="usuarioCita" required>
            <?php
            // Agrega opciones para cada funcionario obtenido desde la base de datos
            foreach ($funcionarios as $funcionario) {
              echo '<option value="' . $funcionario['user_key'] . '">' . $funcionario['username'] . '</option>';
            }
            ?>
          </select>
          <input type="hidden" id="userKeyCita" name="userKeyCita" value="">

          <label for="servicioCita">Servicio:</label>
          <select id="servicioCita" name="servicioCita" required>
            <?php
            // Agrega opciones para cada funcionario obtenido desde la base de datos
            foreach ($servicios as $servicio) {
              echo '<option value="' . $servicio['ser_key'] . '">' . $servicio['ser_nombre'] . '</option>';
            }
            ?>
          </select>
          <input type="hidden" id="serKeyCita" name="serKeyCita" value="">

          <label for="fechaCita">Fecha Cita:</label>
          <input type="date" id="fechaCita" name="fechaCita" required>

          <label for="horaInicioCita">Hora Inicio:</label>
          <input type="time" id="horaInicioCita" name="horaInicioCita" required>

          <label for="horaFinCita">Hora Fin:</label>
          <input type="time" id="horaFinCita" name="horaFinCita" required>

          <button type="submit">Registrar</button>
          <button type="button" id="cancelarFormularioCitas">Cancelar</button>
        </form>
      </div>
    </div>

    <div id="listarCitas" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Citas</h2>
        <label for="fechaInicioCitas">Fecha Inicio:</label>
        <input type="date" id="fechaInicioCitas" required>
        <label for="fechaFinCitas">Fecha Fin:</label>
        <input type="date" id="fechaFinCitas" required>
        <button type="button" id="listarCita">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarCitas">Cancelar</button>
        <button type="button" id="exportarCitas">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaCitas">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Servicio</th>
              <th>Fecha Cita</th>
              <th>Hora Inicio</th>
              <th>Hora Fin</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

    <div id="listarReseñas" class="formulario-listar">
      <div class="formulario-contenedor-listar">
        <h2>Listar Reseñas</h2>
        <label for="fechaInicioReseñas">Fecha Inicio:</label>
        <input type="date" id="fechaInicioReseñas" required>
        <label for="fechaFinReseñas">Fecha Fin:</label>
        <input type="date" id="fechaFinReseñas" required>
        <button type="button" id="listarReseña">Generar Reporte</button>
        <button type="button" id="cancelarFormularioListarReseñas">Cancelar</button>
        <button type="button" id="exportarReseñas">Exportar</button>

        <!-- Tabla para mostrar los datos -->
        <table id="tablaReseñas">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Descripción</th>
              <th>Calificación</th>
              <th>Fecha Carga</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>

  </section>

  <script src="/resources/js/admin/admin.js"></script>

</body>

</html>