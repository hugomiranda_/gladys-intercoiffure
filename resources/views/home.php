<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Gladys Intercoiffure</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon 7
    <link href="1/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="/resources/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/resources/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/resources/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="/resources/css/style.css" rel="stylesheet">
</head>

<body>
    <?php
    session_start();
    ?>
    <!-- Topbar Start -->
    <div class="container-fluid bg-light d-none d-lg-block">
        <div class="row py-2 px-lg-5">
            <div class="col-lg-6 text-left mb-2 mb-lg-0">
                <div class="d-inline-flex align-items-center">
                    <small><i class="fa fa-phone-alt mr-2"></i>+595 982 653037</small>
                    <small class="px-3">|</small>
                    <small><i class="fa fa-envelope mr-2"></i>gladys.zunilda@gmail.com</small>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <div class="d-inline-flex align-items-center">
                    <a class="text-primary px-2" href="https://www.facebook.com/profile.php?id=100054347418508" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a class="text-primary px-2" href="https://www.instagram.com/gladysintercoiffure_" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a class="text-primary pl-2" href="https://wa.me/595982653037" target="_blank">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg bg-white navbar-light py-3 py-lg-0 px-lg-5">
            <a class="navbar-brand ml-lg-3">
                <h1 class="m-0 text-primary"><span class="text-dark">Gladys</span> Intercoiffure </h1>
            </a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between px-lg-3" id="navbarCollapse">
                <div class="navbar-nav m-auto py-0">
                    <a href="#" class="nav-item nav-link active scroll-to" data-target="#home-section">Principal</a>
                    <a href="#" class="nav-item nav-link scroll-to" data-target="#about-section">Nosotros</a>
                    <a href="#" class="nav-item nav-link scroll-to" data-target="#service-section">Servicios</a>
                    <a href="#" class="nav-item nav-link scroll-to" data-target="#location-section">Ubicación</a>
                    <?php

                    if (isset($_SESSION['usuario']) and $_SESSION['user_funcionario'] == 1) {
                        echo '<a href="/admin" class="nav-item nav-link">Admin</a>';
                    }
                    ?>
                </div>
                <?php
                if (isset($_SESSION['usuario'])) {
                    // El usuario está autenticado, muestra la página con el botón de logout
                    echo 'Bienvenido, ' . $_SESSION['usuario'] . '!<br>';
                    echo '<small class="px-3"></small>';
                    echo '<form action="/logout" method="post">
                            <button type="submit" class="btn btn-primary">Logout</button>
                          </form>';
                } else {
                    // El usuario no está autenticado, muestra el botón de login
                    echo '<a href="/mainLogin" class="btn btn-primary d-none d-lg-block">Login</a>';
                }
                ?>
            </div>
        </nav>
    </div>
    <!-- Navbar End -->


    <!-- Carousel Start -->
    <div id='home-section' class="container-fluid p-0 mb-5 pb-5">
        <div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#header-carousel" data-slide-to="1"></li>
                <li data-target="#header-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item position-relative active" style="min-height: 100vh;">
                    <img class="position-absolute w-100 h-100" src="/resources/img/carrusel/peinado1.jpg" style="object-fit: cover;">
                    <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">Estilo y Belleza</h6>
                            <h3 class="display-3 text-capitalize text-white mb-3">Peinados Especiales</h3>
                            <p class="mx-md-5 px-5">Con más de 32 años de experiencia, creamos peinados clásicos e innovadores para que luzcas extraordinaria en cada ocasión. Descubre tu estilo único con nosotras
                            </p>
                            <a class="btn btn-outline-light py-3 px-4 mt-3 animate__animated animate__fadeInUp" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno%20para%20realizarme%20un%20peinado" target="_blank">Reservar Turno</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item position-relative" style="min-height: 100vh;">
                    <img class="position-absolute w-100 h-100" src="/resources/img/carrusel/manicure-pedicure.jpg" style="object-fit: cover;">
                    <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">Estilo y Belleza</h6>
                            <h3 class="display-3 text-capitalize text-white mb-3">Manicure y Pedicure</h3>
                            <p class="mx-md-5 px-5">Con más de 32 años de experiencia, transformamos tus manos y pies en verdaderas obras de arte, resaltando tu belleza única para cada ocasión
                            </p>
                            <a class="btn btn-outline-light py-3 px-4 mt-3 animate__animated animate__fadeInUp" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno%20para%20manicure%20y%20pedicure" target="_blank">Reservar Turno</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item position-relative" style="min-height: 100vh;">
                    <img class="position-absolute w-100 h-100" src="/resources/img/carrusel/depilacion.jpg" style="object-fit: cover;">
                    <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h6 class="text-white text-uppercase mb-3 animate__animated animate__fadeInDown" style="letter-spacing: 3px;">Estilo y Belleza</h6>
                            <h3 class="display-3 text-capitalize text-white mb-3">Depilación</h3>
                            <p class="mx-md-5 px-5">Más de 32 años definiendo suavidad con nuestra experiencia en depilación. Transformamos tu piel con técnicas precisas para que te sientas radiante y segura en cada paso</p>
                            <a class="btn btn-outline-light py-3 px-4 mt-3 animate__animated animate__fadeInUp" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno%20para%20depilación" target="_blank">Reservar Turno</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel End -->


    <!-- About Us Start -->
    <div id="about-section" class="container-fluid py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-6 pb-5 pb-lg-0">
                    <img class="img-fluid w-100" src="/resources/img/GI.jpg" alt="">
                </div>
                <div class="col-lg-6">
                    <h6 class="d-inline-block text-primary text-uppercase bg-light py-1 px-2">Sobre Nosotros</h6>
                    <h1 class="mb-4">Forjando Belleza con Experiencia Duradera</h1>
                    <p class="pl-4 border-left border-primary">Con más de tres décadas de dedicación apasionada, somos una familia de expertas en belleza comprometidas en
                        resaltar tu singularidad. En nuestra peluquería, fusionamos tradición con innovación para ofrecerte
                        servicios excepcionales y resultados que hablan por sí mismos.</p>
                    <div class="row pt-3">
                        <div class="col-6">
                            <div class="bg-light text-center p-4">
                                <h3 class="display-4 text-primary" data-toggle="counter-up">32</h3>
                                <h6 class="text-uppercase">Años de Servicio</h6>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="bg-light text-center p-4">
                                <h3 class="display-4 text-primary" data-toggle="counter-up">999</h3>
                                <h6 class="text-uppercase">Clientes Satisfechos</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Us End -->


    <!-- Service Start -->
    <div id="service-section" class="container-fluid px-0 py-5 my-5">
        <div class="row mx-0 justify-content-center text-center">
            <div class="col-lg-6">
                <h6 class="d-inline-block bg-light text-primary text-uppercase py-1 px-2">Nuestros Servicios</h6>
                <h1>Gladys Intercoiffure</h1>
            </div>
        </div>

        <?php
        $servicesUrl = $_ENV['APP_URL'] . '/services'; // URL de tu API que devuelve la información de los servicios
        $dataServices = file_get_contents($servicesUrl); // Obtenemos los datos desde la API
        $services = json_decode($dataServices, true); // Decodificamos el JSON a un array de PHP
        ?>

        <div class="owl-carousel service-carousel">
            <?php foreach ($services as $service) : ?>
                <?php if ($service['ser_estado'] == '1') : ?>
                    <div class="service-item position-relative">
                        <?php
                        // Determinar el nombre del archivo según el tipo de servicio
                        switch ($service['ser_tipo']) {
                            case 'Peinados':
                                $imgSrc = '/resources/img/services/peinado.jpg';
                                break;
                            case 'Lavados':
                                $imgSrc = '/resources/img/services/lavados.jpg';
                                break;
                            case 'Depilaciones':
                                $imgSrc = '/resources/img/services/depilacion-cera.jpg';
                                break;
                            case 'Uñas y Pies':
                                $imgSrc = '/resources/img/services/manicurista.jpg';
                                break;
                            default:
                                $imgSrc = '/resources/img/services/peinado.jpg';
                                break;
                        }
                        ?>
                        <img class="img-fluid" src="<?= $imgSrc ?>" alt="">
                        <div class="service-text text-center">
                            <h4 class="text-white font-weight-medium px-3"><?= $service['ser_nombre'] ?></h4>
                            <p class="text-white px-3 mb-3"><?= $service['ser_descripcion'] ?></p>
                            <div class="w-100 bg-white text-center p-4">
                                <a class="btn btn-primary" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno" target="_blank">Reservar Turno</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- <div class="row justify-content-center bg-appointment mx-0">
            <div class="col-lg-6 py-5">
                <div class="p-5 my-5" style="background: rgba(33, 30, 28, 0.7);">
                    <h1 class="text-white text-center mb-4">Make Appointment</h1>
                    <form>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control bg-transparent p-4" placeholder="Your Name" required="required" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" class="form-control bg-transparent p-4" placeholder="Your Email" required="required" />
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="date" id="date" data-target-input="nearest">
                                        <input type="text" class="form-control bg-transparent p-4 datetimepicker-input" placeholder="Select Date" data-target="#date" data-toggle="datetimepicker"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="time" id="time" data-target-input="nearest">
                                        <input type="text" class="form-control bg-transparent p-4 datetimepicker-input" placeholder="Select Time" data-target="#time" data-toggle="datetimepicker"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="custom-select bg-transparent px-4" style="height: 47px;">
                                        <option selected>Select A Service</option>
                                        <option value="1">Service 1</option>
                                        <option value="2">Service 1</option>
                                        <option value="3">Service 1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-primary btn-block" type="submit" style="height: 47px;">Make Appointment</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
    <!-- Service End -->


    <!-- Open Hours Start -->
    <div id="openhours-section" class="container-fluid py-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-6" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100" src="/resources/img/horarios/horarios.jpg" style="object-fit: cover;">
                    </div>
                </div>
                <div class="col-lg-6 pt-5 pb-lg-5">
                    <div class="hours-text bg-light p-4 p-lg-5 my-lg-5">
                        <h6 class="d-inline-block text-white text-uppercase bg-primary py-1 px-2">Horarios</h6>
                        <h1 class="mb-4">Encuentra tu Momento de Belleza</h1>
                        <p>Garantizamos una experiencia eficiente y de calidad,
                            permitiéndote disfrutar de nuestros servicios de belleza de manera conveniente.
                        </p>
                        <ul class="list-inline">
                            <li class="h6 py-1"><i class="far fa-circle text-primary mr-3"></i>Lunes : 14:00 PM -
                                9:00 PM</li>
                            <li class="h6 py-1"><i class="far fa-circle text-primary mr-3"></i>Martes a Viernes : 9:00 AM -
                                9:00 PM</li>
                            <li class="h6 py-1"><i class="far fa-circle text-primary mr-3"></i>Sábado : 9:00 AM - 8:00
                                PM</li>
                        </ul>
                        <a class="btn btn-primary mt-2" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno" target="_blank">Agendar Turno</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Open Hours End -->


    <!-- Promo Start -->
    <?php
    $apiPromotionsUrl = $_ENV['APP_URL'] . '/promotions';
    $dataPromotions = file_get_contents($apiPromotionsUrl);
    $promotions = json_decode($dataPromotions, true);
    ?>
    <div class="row mx-0 justify-content-center text-center">
        <div class="col-lg-6">
            <h6 class="d-inline-block bg-light text-primary text-uppercase py-1 px-2">Promociones</h6>
        </div>
    </div>
    <div id="pricing-section" class="container-fluid bg-pricing" style="margin: 25px 0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-5" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100" src="/resources/img/services/bella-mujer-rizos-maquillaje_144627-3577.jpg" style="object-fit: cover;">
                    </div>
                </div>
                <div class="col-lg-7 pt-5 pb-lg-5">
                    <div class="pricing-text bg-light p-4 p-lg-5 my-lg-5">
                        <div class="owl-carousel pricing-carousel">
                            <?php foreach ($promotions as $promotion) : ?>
                                <div class="bg-white">
                                    <div class="d-flex align-items-center justify-content-between border-bottom border-primary p-4">
                                        <h1 class="display-4 mb-0" style="font-size: 2.5rem;">
                                            <small class="align-top text-muted font-weight-medium" style="font-size: 22px; line-height: 45px;">Gs.</small><?= $promotion['pro_precio'] ?><small class="align-bottom text-muted font-weight-medium" style="font-size: 10px; line-height: 40px;"></small>
                                        </h1>
                                        <h5 class="text-primary text-uppercase m-0" style="font-size: 15px;"><?= $promotion['pro_tipo'] ?></h5>
                                    </div>
                                    <div class="p-4">
                                        <p style="text-align: center; font-size: 25px;"><?= $promotion['pro_nombre'] ?></p>
                                        <p><i class="fa fa-check text-success mr-2"></i><?= $promotion['pro_descripcion'] ?></p>
                                        <div class="text-center">
                                            <a class="btn btn-primary mt-2" href="https://wa.me/595982653037?text=Buenas!%20Me%20gustaría%20realizar%20una%20reserva%20de%20turno" target="_blank">Agendar Turno</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Promo End -->


    <!-- Team Start -->
    <div id="team-section" class="container-fluid py-5">
        <div class="container pt-5">
            <div class="row justify-content-center text-center" id="team-header">
                <div class="col-lg-6">
                    <h6 class="d-inline-block bg-light text-primary text-uppercase py-1 px-2">Nuestro Equipo</h6>
                    <h1 class="mb-5">Beauty Specialist</h1>
                </div>
            </div>


            <?php
            $apiUrl = $_ENV['APP_URL'] . '/workers';
            $data = file_get_contents($apiUrl);
            $workers = json_decode($data, true);
            ?>

            <div class="row">
                <?php foreach ($workers as $worker) : ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="team position-relative mb-5">
                            <img class="img-fluid" src="<?php echo $worker['user_imagen']; ?>" alt="">
                            <div class="position-relative text-center">
                                <div class="team-text bg-primary text-white">
                                    <h5 class="text-white text-uppercase"><?php echo $worker['user_nombre']; ?></h5>
                                    <p class="m-0"><?php echo $worker['user_descripcion']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- Team End -->


    <!-- Testimonial Start -->
    <?php
    $reviewsUrl = $_ENV['APP_URL'] . '/lastReviews';
    $dataReviews = file_get_contents($reviewsUrl);
    $reviews = json_decode($dataReviews, true);

    $usersUrl = $_ENV['APP_URL'] . '/getClients';
    $dataUser = file_get_contents($usersUrl);
    $clients = json_decode($dataUser, true);
    ?>
    <div id="testimonial-section" class="container-fluid py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-6 pb-5 pb-lg-0">
                    <img class="img-fluid w-100" src="/resources/img/testimonios/testimonio.jpg" alt="">
                </div>
                <div class="col-lg-6">
                    <h6 califacionTestimonioclass="d-inline-block text-primary text-uppercase bg-light py-1 px-2">Testimonios</h6>
                    <h1 class="mb-4" style="font-size: 29px;">¡Comentarios de Nuestros Clientes!</h1>
                    <div class="owl-carousel testimonial-carousel">
                        <?php foreach ($reviews as $review) : ?>
                            <?php
                            // Encontrar el cliente correspondiente al user_key de la revisión
                            $client = array_filter($clients, function ($client) use ($review) {
                                return $client['user_key'] == $review['user_key'];
                            });

                            // Obtener el primer (y único) elemento del array filtrado
                            $client = reset($client);
                            ?>
                            <div class="position-relative">
                                <i class="fa fa-3x fa-quote-right text-primary position-absolute" style="top: -6px; right: 0;"></i>
                                <div class="d-flex align-items-center mb-3">
                                    <div class="ml-3">
                                        <h6 class="text-uppercase"><?= $client['user_nombre'] ?></h6>
                                        <?php
                                        $calificacion = $review['com_calificacion'];
                                        for ($i = 1; $i <= 5; $i++) {
                                            if ($i <= $calificacion) {
                                                echo '<i class="fa fa-star text-warning"></i>';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <p class="m-0"><?= $review['com_descripcion'] ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <a href="#" id="btnAgregarTestimonio" class="btn btn-primary my-2">Agregar Testimonio</a>
                </div>
            </div>
        </div>
    </div>

    <div id="popup" class="popup" style="text-align: center;">
        <h2 style="color: #F9A392; padding-left: 30px;">Cargar Servicios</h2>
        <form class="formulario-contenedor-cargar" id="formCargarServicios" action="/postReviews" method="POST">

            <label for="califacionTestimonio">Calificación:</label>
            <input type="number" id="calificacionTestimonio" name="calificacionTestimonio" required min="1" max="5">

            <label for="comentarioTestimonio">Comentario:</label>
            <input type="textarea" id="comentarioTestimonio" name="comentarioTestimonio" required>

            <button type="submit" style="background: #F9A392; border-color: #F9A392;">Registrar</button>
            <button type="button" style="background: #F9A392; border-color: #F9A392;" id="cancelarFormularioServicios">Cancelar</button>
        </form>
    </div>
    <!-- Testimonial End -->

    <!-- Google Maps Start -->
    <div id="location-section" class="container-fluid py-5">
        <div class="container py-5">
            <div class="row justify-content-center text-center">
                <div class="col-lg-6">
                    <h6 class="d-inline-block bg-light text-primary text-uppercase py-1 px-2">¿Donde Encontrarnos?</h6>
                    <h1 class="mb-5">Gladys Intercoiffure</h1>
                </div>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2144.5121513961694!2d-57.63048409306932!3d-25.316794891954764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da815df006e69%3A0xc1489dae31cd9d07!2sM9MC%2B47P%2C%20Madrinas%20de%20la%20Guerra%20del%20Chaco%2C%20Asunci%C3%B3n!5e0!3m2!1ses!2spy!4v1698009988055!5m2!1ses!2spy" width="1100" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
    <!-- Google Maps End -->

    <!-- Footer Start -->
    <div class="footer container-fluid position-relative bg-dark py-5" style="margin-top: 90px;">
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-6 pr-lg-5 mb-5">
                    <a class="navbar-brand">
                        <h1 class="mb-3 text-white"><span class="text-primary">Gladys</span> Intercoiffure</h1>
                    </a>
                    <p>Transformando Belleza con Pasión y Experiencia desde 1992. Tu destino para
                        el cuidado personalizado y estilizado. ¡Descubre tu mejor versión con nosotros!</p>
                    <p><i class="fa fa-map-marker-alt mr-2"></i>Madrinas del Chaco 2500 c/ Félix Bogado - Barrio San Vicente, Asunción, Paraguay</p>
                    <p><i class="fa fa-phone-alt mr-2"></i>+595 982 653037</p>
                    <p><i class="fa fa-envelope mr-2"></i>gladys.zunilda@gmail.com</p>
                    <div class="d-flex justify-content-start mt-4">
                        <a class="btn btn-lg btn-primary btn-lg-square mr-2" href="https://wa.me/595982653037" target="_blank"><i class="fab fa-whatsapp"></i></a>
                        <a class="btn btn-lg btn-primary btn-lg-square mr-2" href="https://www.facebook.com/profile.php?id=100054347418508" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-lg btn-primary btn-lg-square" href="https://www.instagram.com/gladysintercoiffure_" target="_blank"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 pl-lg-5">
                    <div class="row">
                        <div class="col-sm-6 mb-5">
                            <h5 class="text-white text-uppercase mb-4">Accesos Rapidos</h5>
                            <div class="d-flex flex-column justify-content-start">
                                <a class="text-white-50 mb-2 scroll-to" href="#" data-target="#home-section"><i class="fa fa-angle-right mr-2"></i>Principal</a>
                                <a class="text-white-50 mb-2 scroll-to" href="#" data-target="#about-section"><i class="fa fa-angle-right mr-2"></i>Nosotros</a>
                                <a class="text-white-50 mb-2 scroll-to" href="#" data-target="#service-section"><i class="fa fa-angle-right mr-2"></i>Nuestros Servicios</a>
                                <a class="text-white-50 mb-2 scroll-to" href="#" data-target="#pricing-section"><i class="fa fa-angle-right mr-2"></i>Promociones</a>
                                <a class="text-white-50 scroll-to" href="#" data-target="#location-section"><i class="fa fa-angle-right mr-2"></i>Encuentranos</a>
                            </div>
                        </div>
                        <div class="col-sm-6 mb-5">
                            <h5 class="text-white text-uppercase mb-4">Nuestros Servicios</h5>
                            <div class="d-flex flex-column justify-content-start">
                                <a class="text-white-50 mb-2"><i class="fa fa-angle-right mr-2"></i>Peinados</a>
                                <a class="text-white-50 mb-2"><i class="fa fa-angle-right mr-2"></i>Manicure</a>
                                <a class="text-white-50 mb-2"><i class="fa fa-angle-right mr-2"></i>Pedicure</a>
                                <a class="text-white-50 mb-2"><i class="fa fa-angle-right mr-2"></i>Lavado</a>
                                <a class="text-white-50"><i class="fa fa-angle-right mr-2"></i>Depilación</a>
                            </div>
                        </div>
                        <div class="col-sm-12 mb-5">
                            <h5 class="text-white text-uppercase mb-4" style="text-align: center;">
                                <?= isset($_SESSION['usuario']) ? '¡Danos tu puntuación y opinión!' : '¡Inicia Sesión para darnos tu opinión!'; ?></h5>
                            <div class="w-100" style="text-align: center;">
                                <?php
                                if (isset($_SESSION['usuario'])) {
                                    // El usuario está autenticado, lleva a la seccion de testimonios
                                    echo '<a href="#" class="btn btn-primary px-4 scroll-to" data-target="#testimonial-section">Comentar!</a>';
                                } else {
                                    // El usuario no está autenticado, muestra el botón para loguearse
                                    echo '<a href="/mainLogin" class="btn btn-primary px-4">Iniciar Sesión</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-dark text-light border-top py-4" style="border-color: rgba(256, 256, 256, .15) !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mb-3 mb-md-0">
                    <p class="m-0 text-white">&copy; <a href="#">Gladys Intercoiffure</a>. All Rights Reserved.</p>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <p class="m-0 text-white">Designed by <a href="https://htmlcodex.com">Hugo & Lucas</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="/resources/lib/easing/easing.min.js"></script>
    <script src="/resources/lib/waypoints/waypoints.min.js"></script>
    <script src="/resources/lib/counterup/counterup.min.js"></script>
    <script src="/resources/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="/resources/lib/tempusdominus/js/moment.min.js"></script>
    <script src="/resources/lib/tempusdominus/js/moment-timezone.min.js"></script>
    <script src="/resources/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

    <!-- Template Javascript -->
    <script src="/resources/js/main.js"></script>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        $(document).ready(function() {
            // Manejar clics en enlaces con la clase "scroll-to"
            $(".scroll-to").on('click', function(e) {
                e.preventDefault();

                // Obtener el objetivo del atributo data-target
                var target = $(this).data('target');

                // Animar el scroll hasta el objetivo
                $('html, body').animate({
                    scrollTop: $(target).offset().top
                }, 1000);
            });
        });
    </script>
</body>

</html>