document.addEventListener("DOMContentLoaded", function () {
  var submenuItems = document.querySelectorAll(".has-submenu");

  submenuItems.forEach(function (item) {
    item.addEventListener("click", function (event) {
      event.preventDefault();
      hideAllForms();
      var formularioId = item.getAttribute("data-formulario");
      showMinimizedForm(formularioId);
    });
  });

  function hideAllForms() {
    var formularios = document.querySelectorAll(".formulario-carga, .formulario-listar");
    formularios.forEach(function (formulario) {
      formulario.style.display = "none";
      formulario.classList.remove("minimized");
    });
  }

  function showMinimizedForm(formularioId) {
    var formulario = document.getElementById(formularioId);
    if (formulario) {
      formulario.style.display = "flex";
      formulario.classList.add("minimized");
    }

  // Funcion que carga los funcionarios en dropdown usuarios de Cargar Citas
  cargarUsuariosComisionDropdown();

  function cargarUsuariosComisionDropdown() {
      fetch('/workersCommission')
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("usuarioComision");
           dropdown.innerHTML = "";  // Limpiamos las opciones existentes

            data.forEach(usuarioComision => {
                const option = document.createElement("option");
                option.value = usuarioComision.user_key;
                option.text = usuarioComision.username;
                dropdown.add(option);
            });
            // Agrega un evento change al dropdown para actualizar el valor del campo oculto userkey
            dropdown.addEventListener("change", function () {
              const userkeyInput = document.getElementById("userKeyComision");
              userkeyInput.value = this.value;
         });
        })
        .catch(error => console.error('Error al obtener la lista de funcionarios:', error));
  }


  // Funcion que carga los funcionarios en dropdown usuarios de Cargar Promociones
  cargarServiciosDropdown();

  function cargarServiciosDropdown() {
      fetch('/getServices')
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("servicioPromocion");
           dropdown.innerHTML = "";  // Limpiamos las opciones existentes

            data.forEach(servicioNombre => {
                const option = document.createElement("option");
                option.value = servicioNombre.ser_key;
                option.text = servicioNombre.ser_nombre;
                dropdown.add(option);
            });
            // Agrega un evento change al dropdown para actualizar el valor del campo oculto userkey
            dropdown.addEventListener("change", function () {
              const userkeyInput = document.getElementById("serKeyPromocion");
              userkeyInput.value = this.value;
         });
        })
        .catch(error => console.error('Error al obtener la lista de servicios:', error));
  }


  // Funcion que carga los funcionarios en dropdown usuarios de Cargar Citas
  cargarUsuariosCitasDropdown();

  function cargarUsuariosCitasDropdown() {
      fetch('/getUsers')
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("usuarioCita");
           dropdown.innerHTML = "";  // Limpiamos las opciones existentes

            data.forEach(usuarioCita => {
                const option = document.createElement("option");
                option.value = usuarioCita.user_key;
                option.text = usuarioCita.username;
                dropdown.add(option);
            });
            // Agrega un evento change al dropdown para actualizar el valor del campo oculto userkey
            dropdown.addEventListener("change", function () {
              const userkeyInput = document.getElementById("userKeyCita");
              userkeyInput.value = this.value;
         });
        })
        .catch(error => console.error('Error al obtener la lista de funcionarios:', error));
  }

  cargarServiciosCitasDropdown();

  function cargarServiciosCitasDropdown() {
      fetch('/getServices')
        .then(response => response.json())
        .then(data => {
            const dropdown = document.getElementById("servicioCita");
           dropdown.innerHTML = "";  // Limpiamos las opciones existentes

            data.forEach(servicioNombreCita => {
                const option = document.createElement("option");
                option.value = servicioNombreCita.ser_key;
                option.text = servicioNombreCita.ser_nombre;
                dropdown.add(option);
            });
            // Agrega un evento change al dropdown para actualizar el valor del campo oculto userkey
            dropdown.addEventListener("change", function () {
              const userkeyInput = document.getElementById("serKeyCita");
              userkeyInput.value = this.value;
         });
        })
        .catch(error => console.error('Error al obtener la lista de servicios:', error));
  }

    // Agregar evento de clic al botón de cancelar Funcionarios
    var cancelarFuncionariosButton = formulario.querySelector("#cancelarFormularioFuncionarios");
    if (cancelarFuncionariosButton) {
      cancelarFuncionariosButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar Servicios
    var cancelarServiciosButton = formulario.querySelector("#cancelarFormularioServicios");
    if (cancelarServiciosButton) {
      cancelarServiciosButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar Promociones
    var cancelarPromocionesButton = formulario.querySelector("#cancelarFormularioPromociones");
    if (cancelarPromocionesButton) {
      cancelarPromocionesButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar Citas
    var cancelarCitasButton = formulario.querySelector("#cancelarFormularioCitas");
    if (cancelarCitasButton) {
      cancelarCitasButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Funcionarios
    var cancelarListarFuncionariosButton = formulario.querySelector("#cancelarFormularioListarFuncionarios");
    if (cancelarListarFuncionariosButton) {
      cancelarListarFuncionariosButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Funcionarios
    var cancelarListarComisionFuncionariosButton = formulario.querySelector("#cancelarFormularioListarComisionFuncionarios");
    if (cancelarListarComisionFuncionariosButton) {
      cancelarListarComisionFuncionariosButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Servicios
    var cancelarListarServiciosButton = formulario.querySelector("#cancelarFormularioListarServicios");
    if (cancelarListarServiciosButton) {
      cancelarListarServiciosButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Promociones
    var cancelarListarPromocionesButton = formulario.querySelector("#cancelarFormularioListarPromociones");
    if (cancelarListarPromocionesButton) {
      cancelarListarPromocionesButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Citas
    var cancelarListarCitasButton = formulario.querySelector("#cancelarFormularioListarCitas");
    if (cancelarListarCitasButton) {
      cancelarListarCitasButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    // Agregar evento de clic al botón de cancelar para listar Reseñas
    var cancelarListarReseñasButton = formulario.querySelector("#cancelarFormularioListarReseñas");
    if (cancelarListarReseñasButton) {
      cancelarListarReseñasButton.addEventListener("click", function () {
        hideAllForms();
      });
    }

    var exportarExcelFuncionariosButton = document.getElementById('exportarFuncionarios');
    if (exportarExcelFuncionariosButton) {
      exportarExcelFuncionariosButton.addEventListener('click', function () {
            exportarTablaExcelFuncionarios('tablaFuncionarios');
        });
    }

    var exportarExcelComisionFuncionariosButton = document.getElementById('exportarComisionFuncionarios');
    if (exportarExcelComisionFuncionariosButton) {
      exportarExcelComisionFuncionariosButton.addEventListener('click', function () {
            exportarTablaExcelComisionFuncionarios('tablaComisionFuncionarios');
        });
    }

    var exportarExcelServiciosButton = document.getElementById('exportarServicios');
    if (exportarExcelServiciosButton) {
      exportarExcelServiciosButton.addEventListener('click', function () {
            exportarTablaExcelServicios('tablaServicios');
        });
    }

    var exportarExcelPromocionesButton = document.getElementById('exportarPromociones');
    if (exportarExcelPromocionesButton) {
      exportarExcelPromocionesButton.addEventListener('click', function () {
            exportarTablaExcelPromociones('tablaPromociones');
        });
    }
  
    var exportarExcelCitasButton = document.getElementById('exportarCitas');
    if (exportarExcelCitasButton) {
      exportarExcelCitasButton.addEventListener('click', function () {
            exportarTablaExcelCitas('tablaCitas');
        });
    } 

    var exportarExcelReseñasButton = document.getElementById('exportarReseñas');
    if (exportarExcelReseñasButton) {
      exportarExcelReseñasButton.addEventListener('click', function () {
            exportarTablaExcelReseñas('tablaReseñas');
        });
    } 

    // Función para exportar la tabla a Excel de Funcionarios
    function exportarTablaExcelFuncionarios(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "funcionarios.xlsx");
    }

    // Función para exportar la tabla a Excel de Comision Funcionarios
    function exportarTablaExcelComisionFuncionarios(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "comision_funcionarios.xlsx");
    }
  
    // Función para exportar la tabla a Excel de Servicios
    function exportarTablaExcelServicios(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "servicios.xlsx");
    }

    // Función para exportar la tabla a Excel de Promociones
    function exportarTablaExcelPromociones(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "promociones.xlsx");
    }

    // Función para exportar la tabla a Excel de Citas
    function exportarTablaExcelCitas(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "citas.xlsx");
    }

    // Función para exportar la tabla a Excel de Reseñas
    function exportarTablaExcelReseñas(idTabla) {
      var table = document.getElementById(idTabla);
      var ws = XLSX.utils.table_to_sheet(table);

      // Crea un libro de trabajo y agrega la hoja de datos
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

      // Descarga el archivo Excel
      XLSX.writeFile(wb, "reseñas.xlsx");
    }
  }

  // Cargar Funcionarios
  document.addEventListener("DOMContentLoaded", function () {
    // Agrega un evento de escucha al formulario
    document.getElementById("formCargarFuncionarios").addEventListener("submit", function (event) {
      event.preventDefault(); // Evita la recarga de la página al enviar el formulario

      // Construye los datos del formulario
      const formData = new FormData(this);

      // Realiza la solicitud POST usando Fetch
      fetch('/postWorkers', {
        method: 'POST',
        body: formData
      })
        .then(response => {
          if (response.ok) {
            document.getElementById("cargarFuncionarios").reset(); // Reinicia el formulario, opcional
          } else {
            console.error('Error en la respuesta:', response.status);
          }
        })
        .catch(error => {
          console.error('Error al enviar la solicitud:', error);
        });
    });
  });

  // Cargar Servicios
  document.addEventListener("DOMContentLoaded", function () {
 
    // Agrega un evento de escucha al formulario
    document.getElementById("formCargarServicios").addEventListener("submit", function (event) {
      event.preventDefault(); // Evita la recarga de la página al enviar el formulario

      // Construye los datos del formulario
      const formData = new FormData(this);

      // Realiza la solicitud POST usando Fetch
      fetch('/postServices', {
        method: 'POST',
        body: formData
      })
        .then(response => {
          if (response.ok) {
            document.getElementById("cargarServicios").reset(); // Reinicia el formulario, opcional
          } else {
            console.error('Error en la respuesta:', response.status);
          }
        })
        .catch(error => {
          console.error('Error al enviar la solicitud:', error);
        });
    });
  });

  // Cargar Promociones
  document.addEventListener("DOMContentLoaded", function () {
 
    // Agrega un evento de escucha al formulario
    document.getElementById("formCargarPromociones").addEventListener("submit", function (event) {
      event.preventDefault(); // Evita la recarga de la página al enviar el formulario

      // Construye los datos del formulario
      const formData = new FormData(this);

      // Realiza la solicitud POST usando Fetch
      fetch('/postPromotions', {
        method: 'POST',
        body: formData
      })
        .then(response => {
          if (response.ok) {
            document.getElementById("cargarPromociones").reset(); // Reinicia el formulario, opcional
          } else {
            console.error('Error en la respuesta:', response.status);
          }
        })
        .catch(error => {
          console.error('Error al enviar la solicitud:', error);
        });
    });
  });

  // Cargar Horarios
  document.addEventListener("DOMContentLoaded", function () {
 
    // Agrega un evento de escucha al formulario
    document.getElementById("formCargarCitas").addEventListener("submit", function (event) {
      event.preventDefault(); // Evita la recarga de la página al enviar el formulario

      // Construye los datos del formulario
      const formData = new FormData(this);

      // Realiza la solicitud POST usando Fetch
      fetch('/postCitas', {
        method: 'POST',
        body: formData
      })
        .then(response => {
          if (response.ok) {
            document.getElementById("cargarCitas").reset(); // Reinicia el formulario, opcional
          } else {
            console.error('Error en la respuesta:', response.status);
          }
        })
        .catch(error => {
          console.error('Error al enviar la solicitud:', error);
        });
    });
  });

  // Ocultar todos los formularios al cargar la página
  hideAllForms();  
});


// Función para actualizar la tabla con los resultados obtenidos de Funcionarios
function actualizarTablaFuncionarios(resultadosFuncionarios) {
  var tablaFuncionarios = document.querySelector('#tablaFuncionarios tbody');
  tablaFuncionarios.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosFuncionarios.forEach(function (funcionario) {
      var row = tablaFuncionarios.insertRow();
      var cellUsuario = row.insertCell(0);
      var cellNombre = row.insertCell(1);
      var cellTelef = row.insertCell(2);
      var cellEmail = row.insertCell(3);
      var cellDescripcion = row.insertCell(4);
      var cellEstado = row.insertCell(5);
      var cellFechaIni = row.insertCell(6);
      var cellFechaFin = row.insertCell(7);

      cellUsuario.textContent = funcionario.username;
      cellNombre.textContent = funcionario.user_nombre;
      cellTelef.textContent = funcionario.user_telef;
      cellEmail.textContent = funcionario.user_email;
      cellDescripcion.textContent = funcionario.user_descripcion;
      cellEstado.textContent = funcionario.user_estado;
      cellFechaIni.textContent = funcionario.user_fechaini;
      cellFechaFin.textContent = funcionario.user_fechafin;
  });
}

function actualizarTablaComisionFuncionarios(resultadosComisionFuncionarios) {
  var tablaComisionFuncionarios = document.querySelector('#tablaComisionFuncionarios tbody');
  tablaComisionFuncionarios.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosComisionFuncionarios.forEach(function (comisionFuncionario) {
    var row = tablaComisionFuncionarios.insertRow();
    var cellUsuario = row.insertCell(0);
    var cellServicio = row.insertCell(1);
    var cellTipo = row.insertCell(2);
    var cellPrecio = row.insertCell(3);
    var cellComision = row.insertCell(4);
    var cellPromocion = row.insertCell(5);
    var cellComisionServicio = row.insertCell(6);
    var cellComisionPromocion = row.insertCell(7);
    var cellFecha = row.insertCell(8);

    cellUsuario.textContent = comisionFuncionario.username;
    cellServicio.textContent = comisionFuncionario.ser_nombre;
    cellTipo.textContent = comisionFuncionario.ser_tipo;
    cellPrecio.textContent = comisionFuncionario.ser_precio;
    cellComision.textContent = comisionFuncionario.ser_comision;
    cellPromocion.textContent = comisionFuncionario.ser_promocion;  // Cambiado<
    cellComisionServicio.textContent = comisionFuncionario.comision_servicio;  // Cambiado
    cellComisionPromocion.textContent = comisionFuncionario.comision_promocion;  // Cambiado
    cellFecha.textContent = comisionFuncionario.cita_fecha;  // Cambiado
  });
}


// Función para actualizar la tabla con los resultados obtenidos de Servicios
function actualizarTablaServicios(resultadosServicios) {
  var tablaServicios = document.querySelector('#tablaServicios tbody');
  tablaServicios.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosServicios.forEach(function (servicio) {
      var row = tablaServicios.insertRow();
      var cellNombre = row.insertCell(0);
      var cellTipo = row.insertCell(1);
      var cellDescripcion = row.insertCell(2);
      var cellPrecio = row.insertCell(3);
      var cellComision = row.insertCell(4);
      var cellHoraEstimada = row.insertCell(5);
      var cellPromocion = row.insertCell(6);
      var cellFechaServicio = row.insertCell(7);

      cellNombre.textContent = servicio.ser_nombre;
      cellTipo.textContent = servicio.ser_tipo;
      cellDescripcion.textContent = servicio.ser_descripcion;
      cellPrecio.textContent = servicio.ser_precio;
      cellComision.textContent = servicio.ser_comision;
      cellHoraEstimada.textContent = servicio.ser_horaesti;
      cellPromocion.textContent = servicio.ser_promocion;
      cellFechaServicio.textContent = servicio.ser_fechaini;
  });
}

// Función para actualizar la tabla con los resultados obtenidos de Promociones
function actualizarTablaPromociones(resultadosPromociones) {
  var tablaPromociones = document.querySelector('#tablaPromociones tbody');
  tablaPromociones.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosPromociones.forEach(function (promocion) {
      var row = tablaPromociones.insertRow();
      var cellServicio = row.insertCell(0);
      var cellNombre = row.insertCell(1);
      var cellTipo = row.insertCell(2);
      var cellDescuento = row.insertCell(3);
      var cellPrecio = row.insertCell(4);
      var cellDescripcion = row.insertCell(5);
      var cellFechaIniPromo = row.insertCell(6);
      var cellFechaFinPromo = row.insertCell(7);

      cellServicio.textContent = promocion.ser_nombre;
      cellNombre.textContent = promocion.pro_nombre;
      cellTipo.textContent = promocion.pro_tipo;
      cellDescuento.textContent = promocion.pro_descuento;
      cellPrecio.textContent = promocion.pro_precio;
      cellDescripcion.textContent = promocion.pro_descripcion;
      cellFechaIniPromo.textContent = promocion.pro_fechaini;
      cellFechaFinPromo.textContent = promocion.pro_fechafin;
  });
}

// Función para actualizar la tabla con los resultados obtenidos de Citas
function actualizarTablaCitas(resultadosCitas) {
  var tablaCitas = document.querySelector('#tablaCitas tbody');
  tablaCitas.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosCitas.forEach(function (cita) {
      var row = tablaCitas.insertRow();
      var cellUsuario = row.insertCell(0);
      var cellServicio = row.insertCell(1);
      var cellFecha = row.insertCell(2);
      var cellHoraIni = row.insertCell(3);
      var cellHoraFin = row.insertCell(4);

      cellUsuario.textContent = cita.username;
      cellServicio.textContent = cita.ser_nombre;
      cellFecha.textContent = cita.cita_fecha;
      cellHoraIni.textContent = cita.cita_horaini;
      cellHoraFin.textContent = cita.cita_horafin;
  });
}

// Función para actualizar la tabla con los resultados obtenidos de Reseñas
function actualizarTablaReseñas(resultadosReseñas) {
  var tablaReseñas = document.querySelector('#tablaReseñas tbody');
  tablaReseñas.innerHTML = '';

  // Agrega filas con los datos obtenidos
  resultadosReseñas.forEach(function (reseña) {
      var row = tablaReseñas.insertRow();
      var cellUsuario = row.insertCell(0);
      var cellDescripcion = row.insertCell(1);
      var cellCalificacion = row.insertCell(2);
      var cellFechaCarga = row.insertCell(3);

      cellUsuario.textContent = reseña.username;
      cellDescripcion.textContent = reseña.com_descripcion;
      cellCalificacion.textContent = reseña.com_calificacion;
      cellFechaCarga.textContent = reseña.com_fecha;
  });
}

// Función para limpiar la tabla Funcionarios
function limpiarTablaFuncionarios() {
  var tablaFuncionarios = document.querySelector('#tablaFuncionarios tbody');
  tablaFuncionarios.innerHTML = '';
}

// Función para limpiar la tabla Comision Funcionarios
function limpiarTablaComisionFuncionarios() {
  var tablaComisionFuncionarios = document.querySelector('#tablaComisionFuncionarios tbody');
  tablaComisionFuncionarios.innerHTML = '';
}

// Función para limpiar la tabla Servicios
function limpiarTablaServicios() {
  var tablaServicios = document.querySelector('#tablaServicios tbody');
  tablaServicios.innerHTML = '';
}

// Función para limpiar la tabla Promociones
function limpiarTablaPromociones() {
  var tablaPromociones = document.querySelector('#tablaPromociones tbody');
  tablaPromociones.innerHTML = '';
}

// Función para limpiar la tabla Citas
function limpiarTablaCitas() {
  var tablaCitas = document.querySelector('#tablaCitas tbody');
  tablaCitas.innerHTML = '';
}

// Función para limpiar la tabla Reseñas
function limpiarTablaReseñas() {
  var tablaReseñas = document.querySelector('#tablaReseñas tbody');
  tablaReseñas.innerHTML = '';
}


// Función ejecuta al cargar la página de Listar Funcionarios
function inicializarPaginaFuncionarios() {

  // Limpia la tabla al cargar la página
  limpiarTablaFuncionarios();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarFuncionario').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaFuncionarios();

    // Obteniene las fechas del formulario
    var fechaInicioFuncionarios = document.getElementById('fechaInicioFuncionarios').value;
    var fechaFinFuncionarios = document.getElementById('fechaFinFuncionarios').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listWorkers', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosFuncionarios = JSON.parse(xhr.responseText);
            actualizarTablaFuncionarios(resultadosFuncionarios);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('fechaInicioFuncionarios=' + fechaInicioFuncionarios + '&fechaFinFuncionarios=' + fechaFinFuncionarios);

    // Limpia los campos del formulario
    document.getElementById('fechaInicioFuncionarios').value = '';
    document.getElementById('fechaFinFuncionarios').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarFuncionarios').addEventListener('click', function () {
    limpiarTablaFuncionarios();
  });
}

// Función ejecuta al cargar la página de Listar Funcionarios
function inicializarPaginaComisionFuncionarios() {

  // Limpia la tabla al cargar la página
  limpiarTablaComisionFuncionarios();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarComisionFuncionario').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaComisionFuncionarios();

    // Obteniene las fechas del formulario
    var usuarioComision = document.getElementById('usuarioComision').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listCommissionWorkers', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosComisionFuncionarios = JSON.parse(xhr.responseText);
            actualizarTablaComisionFuncionarios(resultadosComisionFuncionarios);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('usuarioComision=' + usuarioComision);

    // Limpia los campos del formulario
    document.getElementById('usuarioComision').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarComisionFuncionarios').addEventListener('click', function () {
    limpiarTablaComisionFuncionarios();
  });
}

// Función ejecuta al cargar la página de Listar Servicios
function inicializarPaginaServicios() {

  // Limpia la tabla al cargar la página
  limpiarTablaServicios();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarServicio').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaServicios();

    // Obteniene las fechas del formulario
    var fechaInicioServicios = document.getElementById('fechaInicioServicios').value;
    var fechaFinServicios = document.getElementById('fechaFinServicios').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listServices', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosServicios = JSON.parse(xhr.responseText);
            actualizarTablaServicios(resultadosServicios);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('fechaInicioServicios=' + fechaInicioServicios + '&fechaFinServicios=' + fechaFinServicios);

    // Limpia los campos del formulario
    document.getElementById('fechaInicioServicios').value = '';
    document.getElementById('fechaFinServicios').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarServicios').addEventListener('click', function () {
    limpiarTablaServicios();
  });
}

// Función ejecuta al cargar la página de Listar Promociones
function inicializarPaginaPromociones() {

  // Limpia la tabla al cargar la página
  limpiarTablaPromociones();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarPromocion').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaPromociones();

    // Obteniene las fechas del formulario
    var fechaInicioPromociones = document.getElementById('fechaInicioPromociones').value;
    var fechaFinPromociones = document.getElementById('fechaFinPromociones').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listPromotions', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosPromociones = JSON.parse(xhr.responseText);
            actualizarTablaPromociones(resultadosPromociones);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('fechaInicioPromociones=' + fechaInicioPromociones + '&fechaFinPromociones=' + fechaFinPromociones);

    // Limpia los campos del formulario
    document.getElementById('fechaInicioPromociones').value = '';
    document.getElementById('fechaFinPromociones').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarPromociones').addEventListener('click', function () {
    limpiarTablaPromociones();
  });
}

// Función ejecuta al cargar la página de Listar Citas
function inicializarPaginaCitas() {

  // Limpia la tabla al cargar la página
  limpiarTablaCitas();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarCita').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaCitas();

    // Obteniene las fechas del formulario
    var fechaInicioCitas = document.getElementById('fechaInicioCitas').value;
    var fechaFinCitas = document.getElementById('fechaFinCitas').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listCitas', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosCitas = JSON.parse(xhr.responseText);
            actualizarTablaCitas(resultadosCitas);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('fechaInicioCitas=' + fechaInicioCitas + '&fechaFinCitas=' + fechaFinCitas);

    // Limpia los campos del formulario
    document.getElementById('fechaInicioCitas').value = '';
    document.getElementById('fechaFinCitas').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarCitas').addEventListener('click', function () {
    limpiarTablaCitas();
  });
}

// Función ejecuta al cargar la página de Listar Reseñas
function inicializarPaginaReseñas() {

  // Limpia la tabla al cargar la página
  limpiarTablaReseñas();

  // Registra el evento al hacer click al botón Generar Reporte
  document.getElementById('listarReseña').addEventListener('click', function () {
    // Limpiar la tabla antes de realizar la solicitud AJAX
    limpiarTablaReseñas();

    // Obteniene las fechas del formulario
    var fechaInicioReseñas = document.getElementById('fechaInicioReseñas').value;
    var fechaFinReseñas = document.getElementById('fechaFinReseñas').value;

    // Realiza una solicitud AJAX al controlador
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/listReviews', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          try {
            // Actualiza la tabla con los resultados
            var resultadosReseñas = JSON.parse(xhr.responseText);
            actualizarTablaReseñas(resultadosReseñas);
          } catch (error) {
            console.error("Error al procesar la respuesta JSON:", error);
          }
        } else {
          console.error("Error en la solicitud AJAX. Estado:", xhr.status);
        }
      }
    };
    xhr.send('fechaInicioReseñas=' + fechaInicioReseñas + '&fechaFinReseñas=' + fechaFinReseñas);

    // Limpia los campos del formulario
    document.getElementById('fechaInicioReseñas').value = '';
    document.getElementById('fechaFinReseñas').value = '';
  });

  // Registra el evento clic al botón Cancelar
  document.getElementById('cancelarFormularioListarReseñas').addEventListener('click', function () {
    limpiarTablaReseñas();
  });
}

// Función que se ejecuta al salir de la página Funcionarios
function limpiarPaginaFuncionarios() {
  // Limpiar la tabla al salir de la página
  limpiarTablaFuncionarios();
}

// Función que se ejecuta al salir de la página Comision Funcionarios
function limpiarPaginaComisionFuncionarios() {
  // Limpiar la tabla al salir de la página
  limpiarTablaComisionFuncionarios();
}

// Función que se ejecuta al salir de la página Servicios
function limpiarPaginaServicios() {
  // Limpiar la tabla al salir de la página
  limpiarTablaServicios();
}

// Función que se ejecuta al salir de la página Promociones
function limpiarPaginaPromociones() {
  // Limpiar la tabla al salir de la página
  limpiarTablaPromociones();
}

// Función que se ejecuta al salir de la página Citas
function limpiarPaginaCitas() {
  // Limpiar la tabla al salir de la página
  limpiarTablaCitas();
}

// Función que se ejecuta al salir de la página Reseñas
function limpiarPaginaReseñas() {
  // Limpiar la tabla al salir de la página
  limpiarTablaReseñas();
}



// Llama a la función de inicialización al cargar la página
document.addEventListener('DOMContentLoaded', inicializarPaginaFuncionarios);
document.addEventListener('DOMContentLoaded', inicializarPaginaComisionFuncionarios);
document.addEventListener('DOMContentLoaded', inicializarPaginaServicios);
document.addEventListener('DOMContentLoaded', inicializarPaginaPromociones);
document.addEventListener('DOMContentLoaded', inicializarPaginaCitas);
document.addEventListener('DOMContentLoaded', inicializarPaginaReseñas);

// Llama a la función de reseteo al salir de la pagina
window.addEventListener('beforeunload', limpiarPaginaFuncionarios);
window.addEventListener('beforeunload', limpiarPaginaComisionFuncionarios);
window.addEventListener('beforeunload', limpiarPaginaServicios);
window.addEventListener('beforeunload', limpiarPaginaPromociones);
window.addEventListener('beforeunload', limpiarPaginaCitas);
window.addEventListener('beforeunload', limpiarPaginaReseñas);




